<?php
require_once 'Autoloader.php';
require_once 'includes\Header.php';
?>

<div class="container-fluid mainContainer">
    <div class="row h-75 align-items-center justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <h5 class="card-header text-center">Welcome to Aleksandar's Clinic Software</h5>
                <div class="card-body">
                    <h5 class="card-title text-center">Please log in to continue</h5>
                    <form method="POST" action="Validate.php">
                        <div class="mb-3">
                            <label for="emailField" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="emailField" aria-describedby="emailHelp" name="emailField">
                        </div>
                        <div class="mb-3">
                            <label for="passwordField" class="form-label">Password</label>
                            <input type="password" class="form-control" id="passwordField" name="passwordField">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'includes\Footer.php'; ?>