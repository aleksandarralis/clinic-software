<?php
    CONST dbDNS = 'localhost';
    CONST dbName = 'clinic_software';
    CONST dbUsername = 'root';
    CONST dbPassword = '';

    try {
        $conn = new PDO('mysql:host='.dbDNS.';dbname='.dbName,dbUsername,dbPassword);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
?>