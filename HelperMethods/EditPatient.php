<?php
require_once 'Db.php';
require_once '../Autoloader.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $patientId = $_POST['id'];
    $patientName = $_POST['name'];
    $patientEmail = $_POST['email'];
    $patientPhone = $_POST['phone'];
    $patientAddress = $_POST['address'];
    $medicalCondition = $_POST['medicalCondition'];
    $bloodType = $_POST['bloodType'];
}
echo Patient::editPatient($conn, $patientId, $patientName, $patientEmail, $patientPhone, $patientAddress, $medicalCondition, $bloodType);
die();
?>