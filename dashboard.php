<?php
session_start();
$_SESSION["status"] = '1';
    require_once 'HelperMethods\Db.php';
    require_once 'Autoloader.php';
    require_once 'includes\Header.php';
?>

<div class="container-fluid">
    <div class="row justify-content-center mt-5">
        <div class="col-md-10 justify-content-center">
            <h5 class="text-center">Search Patient By ID</h5>
            <form action="" id="searchform" >
                <input type="text" name="search" id="searhFormInput" class="form-control">
            </form>
        </div>
    </div>
    <div class="row justify-content-center mt-5">
        <div class="col-md-10 justify-content-center">
            <table class="table" >
                <thead>
                    <tr class="justify-content-center">
                        <th scope="col">Id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Address</th>
                        <th scope="col">Medical Condition</th>
                        <th scope="col">Blood Type</th>
                    </tr>
                </thead>
                <tbody id="mainTable">

                </tbody>
            </table>
        </div>
    </div>
</div>


<script type="module" src="/JS/Dashboard.js"></script>
<?php require_once 'includes\Footer.php'; ?>