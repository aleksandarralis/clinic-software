window.onload = getSpecificPatient();

function getSpecificPatient(){
    const urlParams = new URLSearchParams(window.location.search);
    const myParam = urlParams.get('id');
    console.log(myParam);
    $.ajax({
        type: "POST",
        url: "http://localhost/HelperMethods/GetSpecificPatient.php",
        data: {id:myParam}
    }).done(function(data){
        insertPatientData(data);
        let form = document.getElementById('mainForm');
        let email = document.getElementById('email');
        let phone = document.getElementById('phone');
        let address = document.getElementById('address');
        let medicalCondition = document.getElementById('medicalCondition');
        let bloodType = document.getElementById('bloodType');
        let name = document.getElementById('name');
        form.addEventListener('submit', function(event){
            event.preventDefault();
            $.ajax({
                    type: "POST",
                    url: "http://localhost/HelperMethods/EditPatient.php",
                    data: {
                        id:myParam,
                        name: name.value,
                        phone: phone.value,
                        address: address.value,
                        medicalCondition: medicalCondition.value,
                        bloodType: bloodType.value,
                        email: email.value
                    }
                }).done(function(data){
                    window.location.href = "http://localhost/dashboard.php?" + data;
                });

        });
    });
}

function insertPatientData(patients){
    let form = null; 
    let formContainer = document.getElementById('formContainer');
    patients = JSON.parse(patients);
    console.log(patients);
    patients.forEach(element => {
        
        form = document.createElement("form");
        form.innerHTML = `
        <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" id="name" name='name' value="${element.name}">
                
        </div>
        <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="text" class="form-control" id="email" name="email" value="${element.email}">
        </div>
        <div class="mb-3">
                <label for="phone" class="form-label">Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" value="${element.phone}">
                
        </div>
        <div class="mb-3">
                <label for="address" class="form-label">Address</label>
                <input type="text" class="form-control" id="address" name="address" value="${element.address}">
        </div>
        <div class="mb-3">
                <label for="medicalCondition" class="form-label">Medical Condition</label>
                <input type="text" class="form-control" id="medicalCondition" name="medicalCondition" value="${element.medical_condition}">
                
        </div>
        <div class="mb-3">
                <label for="bloodType" class="form-label">Blood Type</label>
                <input type="text" class="form-control" id="bloodType" name="bloodType" value="${element.blood_type}">
        </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        `
        form.setAttribute('id','mainForm');
        formContainer.appendChild(form);
    });
}