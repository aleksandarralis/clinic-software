window.onload = getAllPatientts();
let table = document.getElementById('mainTable');
let searchForm = document.getElementById('searchform');
let searchFormInput = document.getElementById('searhFormInput');

searchForm.addEventListener('keyup', function(event){
    console.log(searchFormInput.value);
    event.preventDefault();
    if(searchFormInput.value == ''){
        getAllPatientts();
        return
    }
    $.ajax({
        type: "POST",
        url: "http://localhost/HelperMethods/GetSpecificPatient.php",
        data: {id:searchFormInput.value}
    }).done(function(data){
        table.innerHTML = '';
        insertPatientRow(data);
        addEventListeners();
    });
})

function insertPatientRow(patients){
    let row = null; 
    let table = document.getElementById('mainTable');
    table.innerHTML = '';
    if (patients == 'There are no patients in the database'){
        table.innerHTML = `<h5 class="text-center">There is no patient with this id</h5>`
        return;
    }
    patients = JSON.parse(patients);
    patients.forEach(element => {
        
        row = document.createElement("tr");
        row.innerHTML = `<th scope="row">${element.id}</th>
        <td>${element.name}</td>
        <td>${element.email}</td>
        <td>${element.phone}</td>
        <td>${element.address}</td>
        <td>${element.medical_condition}</td>
        <td>${element.blood_type}</td>
        <td><a><button class='btn btn-primary editButton' id='${element.id}'>Edit</button></a></td>
        <td><a><button class="btn btn-danger deleteButton" id='${element.id}'>Delete</button></a></td>`
        table.appendChild(row);

    });
}

function getAllPatientts(){
$.ajax({
    type: "GET",
    url: "http://localhost/HelperMethods/GetAllPatients.php",
}).done(function(data){
    insertPatientRow(data);
    addEventListeners();
    });
}

function addEventListeners(){
    let editButtons = document.getElementsByClassName('editButton');
    let deleteButtons = document.getElementsByClassName('deleteButton');
    editButtons = Array.from(editButtons);
    deleteButtons = Array.from(deleteButtons);
    editButtons.forEach(element => {
        element.addEventListener('click',function(){
            window.location.href = "http://localhost/editspecificpatient.php?id=" + element.id;
        });
    });

    deleteButtons.forEach(element =>{
        element.addEventListener('click', function(){
            if(confirm('Are you sure you want to delete?')){
                $.ajax({
                        type: "POST",
                        url: "http://localhost/HelperMethods/DeletePatient.php",
                        data: {
                            id:element.id
                        }
                }).done(function(data){
                        window.location.href = "http://localhost/dashboard.php";
                });
            }
        });
    });
}




