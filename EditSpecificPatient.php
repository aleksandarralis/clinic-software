<?php
session_start();
$_SESSION["status"] = '1';
    require_once 'HelperMethods\Db.php';
    require_once 'Autoloader.php';
    require_once 'includes\Header.php';
?>

<div class="container-fluid">
    <div class="row justify-content-center mt-5">
        <div class="col-md-10 justify-content-center" id="formContainer">
        </div>
    </div>
</div>


<script type="module" src="/JS/EditPatient.js"></script>
<?php require_once 'includes\Footer.php'; ?>