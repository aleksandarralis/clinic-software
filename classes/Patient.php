<?php
    require_once 'Db.php';
Class Patient
{
    public static function getAllPatients($conn)
    {
        $stmt = $conn->prepare("SELECT * FROM patients WHERE 1");  
        return self::executeGetPatienQuery($stmt);

    }
    public static function getSpecificPatient($conn, $id)
    {   
        $stmt = $conn->prepare("SELECT * FROM patients WHERE id = $id");
        return self::executeGetPatienQuery($stmt);
    }

    public static function editPatient($conn, $id, $name, $email, $phone, $address, $medicalCondition, $bloodType)
    {
        $stmt = $conn->prepare("UPDATE patients SET name = ?, 
        email = ?, 
        phone = ?, 
        address = ?, 
        medical_condition = ?, 
        blood_type = ? 
        WHERE id = ?");
        $stmt->execute([$name, $email, $phone, $address, $medicalCondition, $bloodType, $id]);
        
        return 'Patient edited successfully';
    }

    public static function executeGetPatienQuery($stmt)
    {
        $stmt->execute();

        $result = $stmt->fetchAll();

        if (!count($result) > 0) {
            return "There are no patients in the database";
        }

        foreach ($result[0] as $key => $value) {
            if (is_int($key)){
                unset($result[0][$key]);
            }
        }
        
        $result = json_encode($result);
    
        return $result;
    }

    public static function deletePatient($conn, $id)
    {
        $stmt = $conn->prepare("DELETE FROM patients WHERE id = ?");
        $stmt->execute([$id]);
        
        return "Patient deleted Successfully";
    }
}