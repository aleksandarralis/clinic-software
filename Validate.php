<?php
require_once 'HelperMethods\Db.php';
require_once 'Autoloader.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $email = $_POST['emailField'];
    $password = $_POST['passwordField'];
    Doctor::login($conn,$email,$password);
}
require_once 'includes\Header.php';
?>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h2 class="text-center">Wrong Credentials</h2>
            <h5 class="text-center"><a href="index.php"><button class="btn btn-primary">Try Again</button></a></h5>
        </div>
    </div>
</div>


<?php require_once 'includes\Footer.php'; ?>